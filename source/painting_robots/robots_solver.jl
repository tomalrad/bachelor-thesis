using DataStructures, BSON, Flux, CUDAnative, CuArrays
using BSON: @load

struct State
    board
    robot_pos
    dist
end

struct Goal
    board
    colors
end

struct Config
    goal
    robots_positions
    debug
    check_steps

end

struct Result
    path
    state
    expanded_count
end

function load_nn()
    Core.eval(Main, :(import NNlib))
    @load "robots_128_3_1_50_model.bson" m
    @load "robots_128_3_1_50_composer.bson" c

    model = m #|> gpu
    composer = c #|> gpu
    return model, composer
end

function load_heur_nn()
    @load "robots_heur_200_onehot_model.bson" m

    atts = []

    @load "att1_onehot.bson" a1
    @load "att2_onehot.bson" a2
    @load "att3_onehot.bson" a3
    @load "att4_onehot.bson" a4
    @load "att5_onehot.bson" a5

    att_block = []
    push!(att_block, a1) #|> gpu)
    push!(att_block, a2) #|> gpu)
    push!(att_block, a3) #|> gpu)
    push!(att_block, a4) #|> gpu)
    push!(att_block, a5) #|> gpu)
    push!(atts, att_block)

    model = m #|> gpu   
   
    return model, atts
end


heur_model, atts = load_heur_nn()
apply_atts = (x, atts) -> cat([x .* a(x) for a in atts]...,x,dims=3)
model, composer = load_nn()


function solve(config)
    discovered_count = 0
    q = PriorityQueue{State, Float64}()
    parents = Dict()
    visited = []

    start_state = State(zeros(Int64, size(config.goal.board)), config.robots_positions, 0)
    parents[start_state] = 0

    enqueue!(q, start_state, 0)
    push!(visited, start_state)

    iter = 0
    while length(q) > 0
        current = dequeue!(q)
        iter = iter + 1
        discovered_count = discovered_count + 1
        if iter > 20000
            return Result([],State([], [], 0), discovered_count)
        end

        if config.debug
            #print_board(current.board)
            #println(current.robot_pos)
            #println("==============================")
        end
        #println(current.board)

        if current.board == config.goal.board
            path = reconstruct_path(parents, current)
            return Result(path, current, discovered_count)
        end

        next_states = get_next_states(current, config.goal.colors)

        for state in next_states
            if !(contains_state(visited, state))
                valid = true
                for i in CartesianIndices(state.board)
                    if state.board[i] != 0 && config.goal.board[i] == 0 || (state.board[i] != 0 && config.goal.board[i] != state.board[i])
                        valid = false
                    end
                end
                if valid
                    heur = compute_heuristic_bad(state.board, config.goal.board)
                    enqueue!(q, state, 0)
                    parents[state] = current
                    push!(visited, state)
                end
            end
            
        end

    end
    return Result([],State([], [], 0), discovered_count)
end

function transform_for_heur(goal, board)
    res = zeros(size(goal))
    for i in CartesianIndices(goal)
        if goal[i] == 1 && board[i] == 0
            res[i] = 1
        end
    end
    return res
end

function solve_nn(config)
    discovered_count = 0
    q = PriorityQueue{State, Float64}()
    parents = Dict()
    visited = []
    if length(config.robots_positions) == 1
        f_next_states = get_next_states_nn
    else
        f_next_states = get_next_states_multiagent_nn
    end

    start_state = State(zeros(Int64, size(config.goal.board)), config.robots_positions, 0)
    parents[start_state] = 0

    enqueue!(q, start_state, 0)
    push!(visited, start_state)

    iter = 0

    while length(q) > 0
        current = dequeue!(q)
        discovered_count = discovered_count + 1
        if iter > 10000
            return Result([],State([], [], 0), discovered_count)
        end
        iter = iter + 1

        if(config.debug)
            print_board(current.board)
            println(current.robot_pos)
            println("==============================")
        end
        #println(current.board)

        if(current.board == config.goal.board)
            path = reconstruct_path(parents, current)
            return Result(path, current, discovered_count)
        end

        next_states = f_next_states(current, model, composer)

        if config.check_steps
            if !check_steps(current, next_states)
                break
            end
        end

        for state in next_states
            if !(contains_state(visited, state))
                valid = true
                for i in CartesianIndices(state.board)
                    if state.board[i] != 0 && config.goal.board[i] == 0 || (state.board[i] != 0 && config.goal.board[i] != state.board[i])
                        valid = false
                    end
                end
                if valid
                    heur = heur_nn(transform_for_heur(config.goal.board, state.board), apply_atts)
                    enqueue!(q, state, heur)
                    parents[state] = current
                    push!(visited, state)
                end
            end
            
        end

    end
    return Result([],State([], [], 0), 0)
end

function check_steps(current, next_steps)
    for step in next_steps
        for robot in step.robot_pos
            correct = false
            for cur_robot in current.robot_pos
                if is_next_to_or_on_top(robot, cur_robot)
                    if step.board[robot...] == 1
                        println("WRONG MOVE")
                        println(string("Robot at position ", cur_robot, "made incorrect move to ", robot, " with color ", step.board[robot...]))
                        return false
                    elseif  length(step.robot_pos) != length(unique(step.robot_pos))
                        println("WRONG MOVE")
                        println(string("At least two robots made a move to the same square at position ",robot, " state ", step))
                        return false
                    end
                    correct = true
                end
            end
            if !correct
                println("WRONG MOVE")
                println(string("State after move ", step, " State before move", current))
                return false
            end
        end
    end
    return true
end

function get_next_nn(state, model, composer)
    data = reshape(transfrom_for_network(state), size(state.board,1), size(state.board,2), 3, 1)
    r = softmax(resnet(model, composer, data), dims=3)
    return r
end

function resnet(model,composer,x)

    composer(cat(x, model(x), dims = 3))

end


function get_next_states_nn(state, model, composer)
     result = []

     nn_rep = get_next_nn(state, model, composer)

     player_layer = nn_rep[:,:,3,1]
     rob_pos = CartesianIndices(player_layer)[player_layer .> 0.2]

     ones_layer = nn_rep[:,:,2,1]
     ones_pos = CartesianIndices(ones_layer)[ones_layer .> 0.4]

     opt_pos = CartesianIndices(ones_layer)[ones_layer .< 0.8]

     board = zeros(Int64, size(ones_layer))
     board_opt = zeros(Int64, size(ones_layer))
     for one in ones_pos
        board[one] = 1
        if !(one in opt_pos)
            board_opt[one] = 1
        end
     end 

     for rob in rob_pos
        new_state = State(board, [(rob[1], rob[2])], state.dist + 1)
        new_state_opt = State(board_opt, [(rob[1], rob[2])], state.dist + 1)
        push!(result, new_state)
        push!(result, new_state_opt)
     end

     return result
end

function get_next_states_multiagent_nn(state, model, composer)
    result = []

    nn_rep = get_next_nn(state, model, composer)

    player_layer = nn_rep[:,:,3,1]
    rob_pos = CartesianIndices(player_layer)[player_layer .> 0.2]

    ones_layer = nn_rep[:,:,2,1]
    ones_pos = CartesianIndices(ones_layer)[ones_layer .> 0.1]

    opt_pos = CartesianIndices(ones_layer)[ones_layer .< 0.8]

    #intersect ones-pos and opt_pos to find indices of squares that can be painted
    opt_indices = collect(intersect(Set(ones_pos), Set(opt_pos)))

    if length(opt_indices) == 0
        return result
    end

    board = zeros(Int64, size(ones_layer))
    for one in ones_pos
        if !(one in opt_pos)
            board[one] = 1
        end
    end 

    boards = []
    # going trough all possible move and paint configs => convert to binary array, pad is number of robots
    for i in 1:2^length(opt_indices)
        config = digits(i, base=2, pad=length(opt_indices)) |> reverse
        new_board = deepcopy(board)
        for i in length(opt_indices)
            new_board[opt_indices[i]] = config[i]
        end
        push!(boards, new_board)
    end

    # divide next robot positions to arrays for each individial robot
    individual_robot_next_positions = []
    for prev in opt_indices
        positions_for_one_robot = []
        for pos in rob_pos
            if is_next_to_or_on_top(pos, prev)
                push!(positions_for_one_robot, Tuple(pos))
            end
        end
        if length(positions_for_one_robot) == 0 
            push!(positions_for_one_robot, Tuple(prev))
        end
        push!(individual_robot_next_positions, positions_for_one_robot)
    end

    robot_configs = get_combinations(individual_robot_next_positions)
   
    for board in boards
        for robot_config in robot_configs
            if length(robot_config) == length(unique(robot_config))
                push!(result, State(board, robot_config, 0))
            end
        end
    end

    return result
end

function get_combinations(arr)
    result = []
    n = length(arr)
    indices = ones(Int16, n)
    while true
        cur = []
        for i in 1:n
            push!(cur, arr[i][indices[i]])
        end
        push!(result, cur)

        next = n
        while next >= 1 && (indices[next] >= length(arr[next]))
            next = next - 1
        end

        if next < 1
            return result
        end

        indices[next] += 1

        for i in next + 1:n
            indices[i] = 1
        end
    end
    return result
end


function is_next_to_or_on_top(robot, position)
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (0, 0)]
    for direction in directions
        if Tuple(robot) .+ direction == Tuple(position)
            return true
        end
    end
    return false
end

function get_next_states(state, colors)
    directions = [(-1, 0), (0, -1), (1, 0), (0, 1)]
    colors = [i for i in 1:colors]
    next_boards = []
    for robot_ind in CartesianIndices(state.robot_pos)
        for dir in directions
            next_positions = deepcopy(state.robot_pos)
            next_position = state.robot_pos[robot_ind] .+ dir
            next_positions[robot_ind] = next_position
            #state with positions of all robots
            next_state = State(state.board, next_positions, state.dist+1)
            #checking only for one robot
            if can_move_in_dir(State(state.board, next_position, state.dist+1)) && !(next_position in state.robot_pos)
                #move without coloring
                push!(next_boards, next_state)
                #moves with color
                for color in colors
                    next_board = deepcopy(state.board)
                    next_board[state.robot_pos[robot_ind]...] = color
                    next_state = State(next_board, next_positions, state.dist+1)
                    push!(next_boards, next_state)
                end
            end
        end
    end

    return next_boards
end

function compute_heuristic_bad(current, goal)
    return sum(goal) - sum(current)
end

function heur_nn(board, apply_atts)
    one_hot = transfrom_for_network_heur(board)
    data = reshape(one_hot, size(one_hot,1), size(one_hot,2), :, 1) #|> gpu
    #CuArrays.allowscalar(false) 
    heur_nn_res = heur_model(data)
    #CuArrays.allowscalar(true) 
    return heur_nn_res[1]
end

function compute_heuristic(current, goal, robot_pos)
    heur_board = zeros(Int16, size(current))
    for i in CartesianIndices(current)
        if current[i] != goal[i]
            heur_board[i] = 1
        end
    end

    not_painted = CartesianIndices(heur_board)[heur_board .= 1]
    max_distances = []
    for robot in robot_pos
        distance = (pos -> euclidean_distance(robot, Tuple(pos)))
        distances = map(distance, not_painted)
        push!(max_distances, maximum(distances))
    end
    
    return minimum(max_distances)
end

function euclidean_distance(robot, tile)
    return sqrt((robot[1] - tile[1])^2 + (robot[2] - tile[2])^2)
end



function is_out_of_bounds(state)
    if state.robot_pos[1] < 1 || state.robot_pos[1] > size(state.board)[1]
        return true
    elseif state.robot_pos[2] < 1 || state.robot_pos[2] > size(state.board)[1]
        return true
    end
    return false
end

function can_move_in_dir(state)
    if is_out_of_bounds(state)
        return false
    elseif state.board[state.robot_pos...] != 0
        return false
    end
    return true
end

function contains_state(array, state)
    for arr_state in array
        if arr_state.board == state.board && is_same_array(arr_state.robot_pos,state.robot_pos)
            return true
        end
    end
    return false
end

function is_same_array(arr1, arr2)
    for elem in arr1
        if !(elem in arr2)
            return false
        end
    end
    return true
end

function reconstruct_path(parents, goal_state)
    path = []
    push!(path, goal_state)
    current = parents[goal_state]

    while current != 0
        push!(path, current)
        current = parents[current]
    end
    path = reverse(path)
    return path
end

cells = Dict(0 => [1,0,0], 1 => [0,1,0], 2 => [0,0,1])

function transfrom_for_network(state)
    x = deepcopy(state.board)
    for robot in state.robot_pos
        x[robot...] = 2
    end
    o = zeros(Float32, size(x)..., 3)
    for ii in CartesianIndices(x)
        o[ii,:,:,:] .= cells[x[ii]]
    end
    return o
end

cells_h = Dict(0 => [1,0], 1 => [0,1])

function transfrom_for_network_heur(state)
    x = deepcopy(state)
    o = zeros(Float32, size(x)..., 2)
    for ii in CartesianIndices(x)
        o[ii,:,:,:] .= cells_h[x[ii]]
    end
    return o
end

function print_board(board)
    for row in eachrow(board)
        println(row)
    end
    println("")
end

function check_solution_correct(solution, config)
    path = solution.path
    if solution.state.board != config.goal.board
        return false
    end
    for i in 2:length(path)
        next = get_next_states(path[i-1], config.goal.colors)
        if !contains_state(next,path[i])
            println("WRONG MOVE")
            println(path[i-1])
            println(path[i])
            return false
        end
    end
    return true
end

function check_multiagent_solution_correct(solution, config)
    path = solution.path
    #check if final state reached via search is the same as goal
    if solution.state.board != config.goal.board
        #println("SOLUTION INCORRECT")
        #println("Result state is not the same as goal state")
        #print_board(solution.state.board)
        #print_board(config.goal.board)
        return false
    end
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (0, 0)]
    for i in 1:length(path)-1
        #check if two robots are on the same square
        if length(path[i].robot_pos) != length(unique(path[i].robot_pos))
            println("SOLUTION INCORRECT")
            println("There are at least two robots on the same square")
            return false
        end
        #chek if the number of robots stayed the same
        if length(path[i].robot_pos) != length(path[i+1].robot_pos)
            println("SOLUTION INCORRECT")
            println(string("The number of robots has changed form ", length(path[i].robot_pos), " to ", length(path[i+1].robot_pos)))
            return false
        end
        for robot_i in 1:length(path[i].robot_pos)
            cur_pos = path[i].robot_pos[robot_i]
            #check if robot made a move to painted square
            if path[i].board[cur_pos...] != 0
                println("SOLUTION INCORRECT")
                println(string("Robot at position ", cur_pos, " made a move to square with color ", path[i].board[cur_pos...]))
                return false
            end
            next_move_pos = path[i+1].robot_pos
            #check if robot made a move in one of the four allowed directions, or stayed at the same square
            correct_pos = false
            for dir in directions
                next_pos = cur_pos .+ dir
                for move_pos in next_move_pos
                    #also check if the next move is valid in terms of color
                    if next_pos == move_pos && path[i].board[next_pos...] == 0
                        correct_pos = true
                        break
                    end
                end
                if correct_pos
                    break
                end
            end
            if !correct_pos
                println("SOLUTION INCORRECT")
                println(string("Robot at position ", cur_pos, " made invalid move to ", next_move_pos))
                return false
            end  
        end
    end
    return true
end

function solve_nn_class(config)
    discovered_count = 0
    q = PriorityQueue{State, Float64}()
    parents = Dict()
    visited = []

    start_state = State(zeros(Int64, size(config.goal.board)), config.robots_positions, 0)
    parents[start_state] = 0
    enqueue!(q, start_state, 0)
    push!(visited, start_state)

    iter = 0
    while length(q) > 0
        current = dequeue!(q)
        discovered_count = discovered_count + 1
        iter = iter + 1
        if iter > 10000
            return Result([],State([], [], 0), discovered_count)
        end

        if config.debug
            print_board(current.board)
            println(current.robot_pos)
            println(compute_heuristic_bad(current.board, config.goal.board) + current.dist)
            println(compute_heuristic_bad(current.board, config.goal.board))
            println(current.dist)
            println("==============================")
        end
        #println(current.board)

    

        next_states = get_next_states_multiagent_nn(current, model, composer)

        for state in next_states
            if !(contains_state(visited, state))
                valid = true
                for i in CartesianIndices(state.board)
                    if state.board[i] != 0 && config.goal.board[i] == 0 || (state.board[i] != 0 && config.goal.board[i] != state.board[i])
                        valid = false
                    end
                end
                if valid
                    if state.board == config.goal.board
                        parents[state] = current
                        path = reconstruct_path(parents, state)
                        return Result(path, state, discovered_count)
                    end
                    heur = compute_heuristic_bad(state.board, config.goal.board)
                    enqueue!(q, state, 0)
                    parents[state] = current
                    push!(visited, state)
                end
            end
            
        end

    end
    return Result([],State([], [], 0), discovered_count)
end

function solve_class_nn(config)
    discovered_count = 0
    q = PriorityQueue{State, Float64}()
    parents = Dict()
    visited = []

    start_state = State(zeros(Int64, size(config.goal.board)), config.robots_positions, 0)
    parents[start_state] = 0

    enqueue!(q, start_state, 0)
    push!(visited, start_state)

    iter = 0

    while length(q) > 0
        current = dequeue!(q)
        discovered_count = discovered_count + 1
        if iter > 10000
            return Result([],State([], [], 0), discovered_count)
        end
        iter = iter + 1

        if(config.debug)
            print_board(current.board)
            println(current.robot_pos)
            println("==============================")
        end
        #println(current.board)

        if(current.board == config.goal.board)
            path = reconstruct_path(parents, current)
            return Result(path, current, discovered_count)
        end

        next_states = get_next_states(current, config.goal.colors)

        if config.check_steps
            if !check_steps(current, next_states)
                break
            end
        end

        for state in next_states
            if !(contains_state(visited, state))
                valid = true
                for i in CartesianIndices(state.board)
                    if state.board[i] != 0 && config.goal.board[i] == 0 || (state.board[i] != 0 && config.goal.board[i] != state.board[i])
                        valid = false
                    end
                end
                if valid
                    heur = heur_nn(transform_for_heur(config.goal.board, state.board), apply_atts)
                    enqueue!(q, state, heur)
                    parents[state] = current
                    push!(visited, state)
                end
            end
            
        end

    end
    return Result([],State([], [], 0), 0)
end
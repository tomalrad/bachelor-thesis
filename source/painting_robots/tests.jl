using Test

include("robots_solver.jl")
include("robots_problem_generator.jl")

function run_all_tests()
    run_not_solve_tests()
    run_solve_tests()
    run_solve_nn_tests()
end

function run_not_solve_tests()
    test_is_out_of_bounds()
    test_can_move_in_dir()
    test_get_next_states()
    test_get_next_states_multiple_robots()
    test_get_next_states_multiple_colors()
    test_get_next_states_multiple_colors_multiple_robots()
    test_robot_collision()
    test_compute_heuristic()
end

function run_solve_tests()
    test_solve_1()
    test_solve_2()
    test_solve_3()
    test_solve_4()
    test_solve_5()
    test_solve_6()
    test_solve_7()
end

function run_solve_nn_tests()
    test_solve_nn_1()
    test_solve_nn_2()
    test_solve_nn_3()
    test_solve_nn_4()
    test_solve_nn_5()
end

function run_solve_nn_compare()
    compare_solve_nn_1()
    compare_solve_nn_2()
    compare_solve_nn_3()
    compare_solve_nn_4()
    compare_solve_nn_5()
end

function test_is_out_of_bounds()

    #given
    should_pass = State(zeros(Int8, (5,5)), (3, 3))
    should_fail_down = State(zeros(Int8, (5,5)), (0, 3))
    should_fail_up = State(zeros(Int8, (5,5)), (6, 3))
    should_fail_right = State(zeros(Int8, (5,5)), (3, 0))
    should_fail_left = State(zeros(Int8, (5,5)), (3, 6))
    
    #then
    @test is_out_of_bounds(should_pass) == false
    @test is_out_of_bounds(should_fail_down) == true
    @test is_out_of_bounds(should_fail_up) == true
    @test is_out_of_bounds(should_fail_right) == true
    @test is_out_of_bounds(should_fail_left) == true

end

function test_can_move_in_dir()

    #given
    board = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]
    out_of_bounds = State(board, (5, 2))
    painted = State(board, (3, 2))
    can_move_left = State(board, (4, 1))
    can_move_right = State(board, (4, 3))

    #then
    @test can_move_in_dir(out_of_bounds) == false
    @test can_move_in_dir(painted) == false
    @test can_move_in_dir(can_move_left) == true
    @test can_move_in_dir(can_move_right) == true

end

function test_get_next_states()
    expected_board_1 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]

    expected_board_2 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 1 0 0;
    ]
    expected = [
        State(expected_board_1, [(4,1)]),
        State(expected_board_2, [(4,1)]), 
        State(expected_board_1, [(4,3)]),   
        State(expected_board_2, [(4,3)])
    ]
    #given
    board = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2)])

    #when
    next_states = get_next_states(state, 1)

    #then
    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end
end

function test_get_next_states_multiple_robots()
    expected_board_1 = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]

    expected_board_2 = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 1 0 0;
    ]

    expected_board_3 = [
        0 0 1 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]
    expected = [
        State(expected_board_1, [(4,1), (1,3)]),
        State(expected_board_2, [(4,1), (1,3)]), 
        State(expected_board_1, [(4,3), (1,3)]),   
        State(expected_board_2, [(4,3), (1,3)]),
        State(expected_board_1, [(4,2), (1,2)]),
        State(expected_board_3, [(4,2), (1,2)]), 
        State(expected_board_1, [(4,2), (1,4)]),   
        State(expected_board_3, [(4,2), (1,4)])
    ]

    #given
    board = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2), (1,3)])

    #when
    next_states = get_next_states(state, 1)

    #then
    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end
end

function test_get_next_states_multiple_colors()
    expected_board_1 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]

    expected_board_2 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 1 0 0;
    ]
    expected_board_3 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 2 0 0;
    ]
    expected = [
        State(expected_board_1, [(4,1)]),
        State(expected_board_2, [(4,1)]),
        State(expected_board_3, [(4,1)]), 
        State(expected_board_1, [(4,3)]),   
        State(expected_board_2, [(4,3)]),
        State(expected_board_3, [(4,3)])
    ]
    #given
    board = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2)])

    #when
    next_states = get_next_states(state, 2)

    #then
    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end
end

function test_get_next_states_multiple_colors_multiple_robots()
    expected_board_1 = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]

    expected_board_2 = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 1 0 0;
    ]
    expected_board_3 = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 2 0 0;
    ]
    expected_board_4 = [
        0 0 1 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]
    expected_board_5 = [
        0 0 2 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]
    expected = [
        State(expected_board_1, [(4,1), (1,3)]),
        State(expected_board_2, [(4,1), (1,3)]),
        State(expected_board_3, [(4,1), (1,3)]), 
        State(expected_board_1, [(4,3), (1,3)]),   
        State(expected_board_2, [(4,3), (1,3)]),
        State(expected_board_3, [(4,3), (1,3)]),
        State(expected_board_1, [(4,2), (1,2)]),
        State(expected_board_4, [(4,2), (1,2)]), 
        State(expected_board_5, [(4,2), (1,2)]), 
        State(expected_board_1, [(4,2), (1,4)]),   
        State(expected_board_4, [(4,2), (1,4)]),
        State(expected_board_5, [(4,2), (1,4)])
    ]
    #given
    board = [
        0 0 0 0;
        0 0 1 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2), (1,3)])

    #when
    next_states = get_next_states(state, 2)

    #then
    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end
end

function test_robot_collision()
    expected_board_1 = [
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
    ]
    expected_board_2 = [
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
        1 0 0 0;
    ]
    expected_board_3 = [
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
    ]
    expected = [
        State(expected_board_1, [(3,1), (4,2)]),
        State(expected_board_2, [(3,1), (4,2)]),
        State(expected_board_1, [(4,1), (3,2)]),
        State(expected_board_3, [(4,1), (3,2)]),
        State(expected_board_1, [(4,1), (4,3)]),
        State(expected_board_3, [(4,1), (4,3)])
    ]

    board = [
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
        0 0 0 0;
    ]

    state = State(board, [(4, 1), (4,2)])

    #when
    next_states = get_next_states(state, 1)

    #then
    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end


end

function test_compute_heuristic()
    goal = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    should_be_16 = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    should_be_8 = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0;
        0 1 0 0 0 0 0 0 0;
        0 1 0 0 0 0 0 0 0;
        0 1 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    @test compute_heuristic(should_be_16, goal) == 16
    @test compute_heuristic(should_be_8, goal) == 8
    @test compute_heuristic(goal, goal) == 0

end

function test_solve_1()

    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(circle, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_multiagent_solution_correct(solution, config)

end

function test_solve_2()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end 


function test_solve_3()

    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 2 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(circle, 2)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)

end

function test_solve_4()
    target = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 2 2 2 1 0 0;
        0 1 2 3 3 3 2 1 0;
        0 1 2 3 4 3 2 1 0;
        0 1 2 3 3 3 2 1 0;
        0 0 1 2 2 2 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 4)

    config = Config(goal, [(1,1), (17,17)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end 

function test_solve_5()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 2 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 2 0 0 0; 
        0 1 0 0 3 0 0 1 0 0 0 0 2 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 2 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 2 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 2 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 2 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 2 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 2 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 2 0 0 0 0 1 0 0 3 0 0 1 0;
        0 0 0 2 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 2 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 3)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end 

function test_solve_6()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 2 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 2 0 0 0; 
        0 1 0 0 3 0 0 1 0 0 0 0 2 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 2 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 2 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 2 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 2 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 2 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 2 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 2 0 0 0 0 1 0 0 3 0 0 1 0;
        0 0 0 2 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 2 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 3)

    config = Config(goal, [(1,1), (17,17)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end 

function test_solve_7()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1), (1,17)], false, false)

    solution = solve(config)

    @test check_multiagent_solution_correct(solution, config)
end

function test_solve_8()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0; 
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 9)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end

function test_solve_9()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 1 0 0 1 0 0 0 0 0 1 0 0 0;
        0 1 0 1 1 1 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 1 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 1 1 1 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 1 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve(config)

    @test check_solution_correct(solution, config)
end

function test_get_next_nn()
    state = State
    model, composer = load_nn()

    board = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2)])

    next = get_next_nn(state, model, composer)

    robot_layer = next[:,:,3,1]

    return robot_layer
end

function test_get_next_states_nn()
    expected_board_1 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]

    expected_board_2 = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 1 0 0;
    ]
    expected = [
        State(expected_board_1, [(4,1)]),
        State(expected_board_2, [(4,1)]), 
        State(expected_board_1, [(4,3)]),   
        State(expected_board_2, [(4,3)])
    ]

    state = State
    model, composer = load_nn()

    board = [
        0 0 0 0;
        0 0 0 0;
        0 1 0 0;
        0 0 0 0;
    ]
    state = State(board, [(4, 2)])

    next_states = get_next_states_nn(state, model, composer)

    @test length(next_states) == length(expected)

    for state in next_states
        @test contains_state(expected, state) == true
    end
end

function test_get_states_nn()
    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    state = State(circle, [(1,2)])
    model, composer = load_nn()

    next_states = get_next_nn(state, model, composer)

    return next_states
end

function test_solve_nn_1()

    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(circle, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve_nn_class(config)

    @test check_solution_correct(solution, config)

end

function test_solve_nn_2()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve_nn_class(config)

    @test check_solution_correct(solution, config)
end

function test_solve_nn_3()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 1 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 1 1 1 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 1 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 1 1 1 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 1 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve_nn_class(config)

    @test check_solution_correct(solution, config)
end

function test_solve_nn_4()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 1 1 1 1 0 0 0 0 0 0 0 1 0 0;
        0 1 1 1 1 1 1 1 0 0 0 0 0 1 0 0 0; 
        0 1 1 1 1 1 1 1 0 0 0 0 1 0 0 0 0;
        0 1 1 1 1 1 1 1 0 0 0 1 0 0 0 0 0;
        0 0 1 1 1 1 1 1 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 0;
        0 0 0 0 0 1 0 0 0 1 1 1 1 1 1 1 0;
        0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 1 0;
        0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 0;
        0 0 1 0 0 0 0 0 0 0 1 1 1 1 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve_nn_class(config)

    @test check_solution_correct(solution, config)
end

function test_solve_nn_5()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0;
        0 0 1 1 1 1 1 0 0 0 0 0 0 0 1 0 0 1 1 1 1 0;
        0 1 1 1 1 1 1 1 0 0 0 0 0 1 0 0 0 1 0 1 0 0;
        0 1 1 1 1 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0 1 0;
        0 1 1 1 1 1 1 1 0 0 0 1 0 0 0 0 0 0 0 0 0 0;
        0 0 1 1 1 1 1 1 0 0 1 0 0 0 0 0 0 1 1 1 1 0;
        0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 0 1 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 0 1 1 1 1 0;
        0 0 0 0 0 1 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 1 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 1 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0;
        0 1 0 1 0 0 1 0 1 1 1 1 0 1 0 1 1 1 1 0 0 0;
        0 0 0 0 1 1 1 0 0 0 1 1 1 1 0 0 0 1 1 1 0 0;
        0 0 0 1 1 1 1 0 1 0 1 0 1 1 1 1 1 1 1 1 0 0;
        0 1 1 1 0 0 1 0 1 0 0 0 0 1 0 0 1 0 1 0 1 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution = solve_nn_class(config)

    @test check_solution_correct(solution, config)
end

function test_solve_nn_6()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1), (17,17)], false, true)

    solution = solve_nn(config)

    @test check_multiagent_solution_correct(solution, config)
end

function test_solve_nn_7()

    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(circle, 1)

    config = Config(goal, [(1,1),(9,9),(1,9),(9,1),(4,4)], true, true)

    solution = solve_nn(config)

    @test check_multiagent_solution_correct(solution, config)

end

function compare_solve_nn_1()

    circle = [
        0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 1 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 1 0 0;
        0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(circle, 1)

    config = Config(goal, [(1,1)], false, false)

    solution_nn = solve_nn(config)
    expanded_nn = solution_nn.expanded_count
    @test check_solution_correct(solution_nn, config)

    solution = solve(config)
    expanded = solution.expanded_count
    @test check_solution_correct(solution, config)

    println(string("Test 1, Expanded NN: ",expanded_nn, " Expanded classic: ", expanded))

end

function compare_solve_nn_2()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 0 0 0 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution_nn = solve_nn(config)
    expanded_nn = solution_nn.expanded_count
    @test check_solution_correct(solution_nn, config)

    solution = solve(config)
    expanded = solution.expanded_count
    @test check_solution_correct(solution, config)

    println(string("Test 2, Expanded NN: ",expanded_nn, " Expanded classic: ", expanded))
end

function compare_solve_nn_3()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0;
        0 1 0 0 1 0 0 1 0 0 0 0 0 1 0 0 0; 
        0 1 0 1 1 1 0 1 0 0 0 0 1 0 0 0 0;
        0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0;
        0 0 1 0 0 0 1 0 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 0 0 0 1 0 0;
        0 0 0 0 0 1 0 0 0 1 0 0 1 0 0 1 0;
        0 0 0 0 1 0 0 0 0 1 0 1 1 1 0 1 0;
        0 0 0 1 0 0 0 0 0 1 0 0 1 0 0 1 0;
        0 0 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution_nn = solve_nn(config)
    expanded_nn = solution_nn.expanded_count
    @test check_solution_correct(solution_nn, config)

    solution = solve(config)
    expanded = solution.expanded_count
    check_solution_correct(solution, config)

    println(string("Test 3, Expanded NN: ",expanded_nn, " Expanded classic: ", expanded))
end

function compare_solve_nn_4()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0;
        0 0 1 1 1 1 1 0 0 0 0 0 0 0 1 0 0;
        0 1 1 1 1 1 1 1 0 0 0 0 0 1 0 0 0; 
        0 1 1 1 1 1 1 1 0 0 0 0 1 0 0 0 0;
        0 1 1 1 1 1 1 1 0 0 0 1 0 0 0 0 0;
        0 0 1 1 1 1 1 1 0 0 1 0 0 0 0 0 0;
        0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 0;
        0 0 0 0 0 1 0 0 0 1 1 1 1 1 1 1 0;
        0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 1 0;
        0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 0;
        0 0 1 0 0 0 0 0 0 0 1 1 1 1 1 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution_nn = solve_nn(config)
    expanded_nn = solution_nn.expanded_count
    @test check_solution_correct(solution_nn, config)

    solution = solve(config)
    expanded = solution.expanded_count
    check_solution_correct(solution, config)

    println(string("Test 4, Expanded NN: ",expanded_nn, " Expanded classic: ", expanded))
end

function compare_solve_nn_5()
    target = [
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0;
        0 0 1 1 1 1 1 0 0 0 0 0 0 0 1 0 0 1 1 1 1 0;
        0 1 1 1 1 1 1 1 0 0 0 0 0 1 0 0 0 1 0 1 0 0;
        0 1 1 1 1 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0 1 0;
        0 1 1 1 1 1 1 1 0 0 0 1 0 0 0 0 0 0 0 0 0 0;
        0 0 1 1 1 1 1 1 0 0 1 0 0 0 0 0 0 1 1 1 1 0;
        0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 0 1 0;
        0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 0 1 1 1 1 0;
        0 0 0 0 0 1 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 0 0 1 0 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 0 1 1 1 1 0;
        0 0 1 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 1 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0;
        0 1 0 1 0 0 1 0 1 1 1 1 0 1 0 1 1 1 1 0 0 0;
        0 0 0 0 1 1 1 0 0 0 1 1 1 1 0 0 0 1 1 1 0 0;
        0 0 0 1 1 1 1 0 1 0 1 0 1 1 1 1 1 1 1 1 0 0;
        0 1 1 1 0 0 1 0 1 0 0 0 0 1 0 0 1 0 1 0 1 0;
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    ]

    goal = Goal(target, 1)

    config = Config(goal, [(1,1)], false, false)

    solution_nn = solve_nn(config)
    expanded_nn = solution_nn.expanded_count
    @test check_solution_correct(solution_nn, config)

    solution = solve(config)
    expanded = solution.expanded_count
    check_solution_correct(solution, config)

    println(string("Test 5, Expanded NN: ",expanded_nn, " Expanded classic: ", expanded))
end

function test_heur_nn()
    heur_nn(generate_random_grid_one_color(9))
    heur_nn(generate_random_grid_one_color(8))
    heur_nn(generate_random_grid_one_color(10))
    heur_nn(generate_random_grid_one_color(11))
end
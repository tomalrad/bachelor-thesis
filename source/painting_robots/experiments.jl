include("robots_solver.jl")
include("robots_problem_generator.jl")

data_robots_7, data_robots_8, data_robots_9 = get_generated_experiment_configs() 


function experiment_7()
    i = 0
    time = 0
    length = 0
    wrong = 0
    expanded = 0
    right = 0
    for config in data_robots_8
        solution = @timed solve_nn_class(config)
        if !check_multiagent_solution_correct(solution[1], config)
            wrong = wrong + 1
        else 
            length = length + size(solution[1].path)[1]
            time = time + solution[2]
            expanded = expanded + solution[1].expanded_count
            right = right + 1
        end
        i += 1
        print(string(i, " "))
    end
    println("")
    println(string("Avg time: ",time/right))
    println(string("Avg Length: ",length/right))
    println(string("Avg expanded: ",expanded/right))
    println(string("Wrong: ", wrong))
end

function experiment_robots_nn_exp_class_heur()
    println("NN expansion, classical heuristic")
    println("Size 10")
    i = 0
    for config in data_robots_10
        solution = @timed solve_nn_class(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        print_board(config.goal.board)
        i += 1
    end
    println("Size 15")
    for config in data_robots_15
        solution = @timed solve_nn_class(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        i += 1
    end
    println("Size 20")
    for config in data_robots_20
        solution = @timed solve_nn_class(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        i += 1
    end
end

function experiment_robots_nn_exp_nn_heur()
    println("NN expansion, NN heuristic")
    println("Size 10")
    i = 0
    for config in data_robots_10
        solution = @timed solve_nn(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        print_board(config.goal.board)
        i += 1
    end
    println("Size 15")
    for config in data_robots_15
        solution = @timed solve_nn(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        i += 1
    end
    println("Size 20")
    for config in data_robots_20
        solution = @timed solve_nn(config)
        println(string("Board:", i," Correct solution: ", check_solution_correct(solution[1], config), " Expanded nodes:", solution[1].expanded_count), " Time: ", solution[2])
        i += 1
    end
end

function experiment_robots_class_exp_class_heur()
end

function experiment_robots_class_exp_nn_heur()
end
using OhMyREPL, Flux, CSV, MLDataUtils, Statistics, LinearAlgebra, DataStructures, Distances, CuArrays, BSON, StatsBase, CUDAnative, Base
using Flux: @epochs, throttle
using BSON: @save 
using JLD: load
import Base.minimum
import Base.maximum


CUDAnative.device!(1) 
CuArrays.allowscalar(true) 

wall_pad = false
imgs = load("robot_one_color_heur.jld")["data"] |> gpu
labels = cu.(load("robot_one_color_heur_labels.jld")["data"]) |> gpu

no_batches = 1000 
batch_size = 60
no_mzs_in_block = 1000
batches = RandomBatches((imgs,labels), 1000, 70) |> gpu
batch_size_identity = zeros(no_mzs_in_block, no_mzs_in_block) + I |> gpu 

function get_attentions(no_atts, no_att_blocks)
    attention_blocks = []
    ps = []
    for i in 1:no_att_blocks
        att = []
        for j in 1:no_atts
            if i == 1 && wall_pad == 1
                l = Chain(Conv((3,3), 2 => 1, swish), x -> softmax(x,dims=[1,2])) |> gpu
            else 
                l = Chain(Conv((3,3), 2 => 1, pad=(1,1), swish), x -> softmax(x,dims=[1,2])) |> gpu
            end
            push!(att, l)
            push!(ps, params(l)...)
        end
        push!(attention_blocks, att)
    end
    return attention_blocks, ps
end

# apply_atts(x, atts) = cat(cat([x .* a(x) for a in atts]..., dims=3), add_coord_filters(x), dims=3) # vraci no_atts * x_filters + x_filters + 2 filtru -> proste to co chceme 

apply_atts(x, atts) = cat([x .* a(x) for a in atts]...,x,dims=3) 

no_atts = 5
att_blocks = 1
atts, ps = get_attentions(no_atts, att_blocks)
no_filters = no_atts * 2 + 2


# no_filters = 8
model = Chain(
    x -> apply_atts(x, atts[1]),
    Conv((1,1), no_filters => 24, swish),
    Conv((3,3), 24 => 24, pad=(1,1), swish),
    Conv((3,3), 24 => 48, pad=(1,1), swish),
    Conv((3,3), 48 => 96, pad=(1,1), swish),
    x -> sum(x, dims=[1,2]),    
    x -> reshape(x, :, size(x)[4]),
    Dense(96, 1),
) |> gpu
push!(ps,params(model)...)

function loss(x,y)
    mx = model(x)
    data_diffs = -1 .* (mx .- transpose(mx))
    label_diffs = sign.(y .- transpose(y))
    mlt = (data_diffs .* label_diffs) .+ 1
    rl = relu.(mlt) 
    tmp = rl .- batch_size_identity
    return sum(tmp)
end

opt = ADAM()
no_epochs = 200
tb = getobs(batches)
println(typeof(tb))
CuArrays.allowscalar(false) 
@epochs no_epochs Flux.train!(loss, ps, tb, opt)
CuArrays.allowscalar(true) 

model_name = string("robots_heur_",no_epochs,"_onehot_model.bson")
m = cpu(model)
@save model_name m

a1 = cpu(atts[1][1])
a2 = cpu(atts[1][2])
a3 = cpu(atts[1][3])
a4 = cpu(atts[1][4])
a5 = cpu(atts[1][5])
@save string("att1_onehot.bson") a1
@save string("att2_onehot.bson") a2
@save string("att3_onehot.bson") a3
@save string("att4_onehot.bson") a4
@save string("att5_onehot.bson") a5
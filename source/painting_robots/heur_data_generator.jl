include("robots_solver.jl")
include("robots_problem_generator.jl")

function generate_heur_data()
    configs = []
    for i in 1:500000
        push!(configs, generate_random_grid_one_color(10))
    end
    boards = zeros(10,10,2,length(configs))
    heurs = []
    for i in 1:length(configs)
        heur = heuristic(configs[i])
        boards[:,:,:,i] = transfrom_for_network_heur(configs[i])
        push!(heurs, heur)
    end
    save("robot_one_color_heur.jld", "data", boards)
    save("robot_one_color_heur_labels.jld", "data", heurs)
end

function  heuristic(board)
    return length(CartesianIndices(board)[board .= 1])
end
using Gtk, Graphics

function plot_robot_path(path)
    win = GtkWindow("Painting robots solution", 800, 800)
    g = GtkGrid()
    
    c = @GtkCanvas(500,500)

    button = GtkButton("Next Step")

    g[1:10,1:10] = c
    g[1:10,11] = button 
    set_gtk_property!(g, :column_homogeneous, true)
    set_gtk_property!(g, :column_spacing, 15)  
    push!(win, g)

    @guarded draw(c) do widget
        ctx = getgc(c)
        h = height(c)
        w = width(c)

        box_width = h/10

        for i in 1:10
            move_to(ctx, (i-1) * box_width, 0)
            line_to(ctx, (i-1) * box_width, w)
        end

        for i in 1:10
            move_to(ctx, 0, (i-1) * box_width)
            line_to(ctx, w, (i-1) * box_width)
        end

        #rectangle(ctx, 0, 0, w, h/2)
        #set_source_rgb(ctx, 1*100*i, 0, 0)
        #fill(ctx)
        #rectangle(ctx, 0, 3h/4, w, h/4)
        #set_source_rgb(ctx, 0, 0, 1)
        fill(ctx)

    end

    show(c)
    showall(win)
end
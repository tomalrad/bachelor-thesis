using JLD

include("robots_solver.jl")

function generate_3x3()
    boards = []
    for i in 0:512
       board = reshape(generate_array(i)[8:end], (3 , 3))
       push!(boards, board)
    end
    return boards
end

function generate_array(i)
    return parse.(Int64 ,split(bitstring(UInt16(i)),""))
end

function generate_states()
    boards = generate_3x3();
    result = []
    for board in boards
        for i in 1:3
            for j in 1:3
                new_state = State(board, [(i,j)])
                push!(result, new_state)
            end
        end 
    end
    return result
end

function generate_steps()
    data = Vector{Vector{Array{Int64,2}}}([])

    states = generate_states()

    for state in states
        state_nn = deepcopy(state.board)
        state_nn[state.robot_pos[1]...] = 2
        next_states = get_next_states(state, 1)
        for next_state in next_states
            next_state_nn = deepcopy(next_state.board)
            next_state_nn[next_state.robot_pos[1]...] = 2
            row = []
            push!(row,state_nn)
            push!(row, next_state_nn)
            push!(data, row)
        end
    end

    return data
end

function save_training_data()
    data = generate_steps()
    imgs = []
    labels = []
    for sample in data 
        push!(imgs, sample[1])
        push!(labels, sample[2])
    end
    final_imgs = zeros(3,3,3,length(imgs))
    final_labels = zeros(3,3,3,length(labels))
    for i in 1:length(imgs)
        m = transfrom_for_network(imgs[i])
        l = transfrom_for_network(labels[i])
        final_imgs[:,:,:,i] = m
        final_labels[:,:,:,i] = l
    end
    save("robots_step.jld", "data", final_imgs)
    save("robots_step_labels.jld", "data", final_labels)
    return final_labels
end

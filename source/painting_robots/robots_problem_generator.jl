using JLD, Random

include("robots_solver.jl")

function generate_problems()
    result = []
    positions = []
    for size in 8:30
        for i in 1:2000*size
            push!(result, generate_random_grid_one_color(size))
            push!(positions, generate_random_position(size))
        end
    end
    save("one_robot_one_color_boards.jld", "data", result)
    save("one_robot_one_color_positions.jld", "data", positions)
end

function get_generated_configs(random_starts)
    boards = load("one_robot_one_color_boards.jld")["data"]
    positions = load("one_robot_one_color_positions.jld")["data"]
    configs = []
    for i in CartesianIndices(boards)
        goal = Goal(boards[i],1)
        if random_starts
            starts = [positions[i]]
        else
            starts = [(1,1)]
        end
        push!(configs ,Config(goal, starts, false, false))
    end
    return configs
end

function generate_random_grid_one_color(size)
    return rand([0,1], size, size)
end

function generate_random_position(size)
    return tuple(rand(1:size,1,2)...)
end

function generate_random_config(size)
    goal = Goal(generate_random_grid_one_color(size),1)
    return Config(goal, [(1,1)], false, false)
end

function test_save()
    res = []
    push!(res, generate_random_grid_one_color(10))
    push!(res, generate_random_grid_one_color(20))
    push!(res, generate_random_grid_one_color(30))
    save("one_robot_one_color_problems.jld", "data", res)
end

function test_load()
    data = load("one_robot_one_color_problems.jld")["data"]
    println(data[1])
end

function get_generated_experiment_configs()
    boards_10 = load("exp_robots_7_nn_multi.jld")["data"]
    boards_15 = load("exp_robots_8_nn_multi.jld")["data"]
    boards_20 = load("exp_robots_9_nn_multi.jld")["data"]
    configs_10 = []
    configs_15 = []
    configs_20 = []
    for i in CartesianIndices(boards_10)
        goal = Goal(boards_10[i],1)
        starts = [(1,1), (7,7)]
        push!(configs_10 ,Config(goal, starts, false, false))
    end
    for i in CartesianIndices(boards_15)
        goal = Goal(boards_15[i],1)
        starts = [(1,1), (8,8)]
        push!(configs_15 ,Config(goal, starts, false, false))
    end
    for i in CartesianIndices(boards_20)
        goal = Goal(boards_20[i],1)
        starts = [(1,1), (9,9)]
        push!(configs_20 ,Config(goal, starts, false, false))
    end
    return configs_10, configs_15, configs_20
end

function save_experiment_data_old()
    data_robots_10 = []
    data_robots_15 = []
    data_robots_20 = []

    for i in 1:50
        push!(data_robots_10, generate_random_grid_one_color(7))
        push!(data_robots_15, generate_random_grid_one_color(8))
        push!(data_robots_20, generate_random_grid_one_color(9))
    end
    save("exp_robots_7.jld", "data", data_robots_10)
    save("exp_robots_8.jld", "data", data_robots_15)
    save("exp_robots_9.jld", "data", data_robots_20)
end

function save_experiment_data()
    data_robots_7 = []
    data_robots_8 = []
    data_robots_9 = []

    i = 0
    while i != 20
        board = generate_random_grid_one_color(8)
        goal = Goal(board,1)
        starts = [(1,1), (8,8)]
        config = Config(goal, starts, false, false)
        if check_multiagent_solution_correct(solve_nn_class(config), config)
            push!(data_robots_8, board)
            i = i + 1
            print(string(i, " "))
        end
    end
    save("exp_robots_8_nn_multi.jld", "data", data_robots_8)

    i = 0
    while i != 20
        board = generate_random_grid_one_color(9)
        goal = Goal(board,1)
        starts = [(1,1), (9,9)]
        config = Config(goal, starts, false, false)
        if check_multiagent_solution_correct(solve_nn_class(config), config)
            push!(data_robots_9, board)
            i = i + 1
            print(string(i, " "))
        end
    end
    save("exp_robots_9_nn_multi.jld", "data", data_robots_9)
end

function generate_9by9()
    boards = []

    board = [
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 1 0 0 0 1 0 0;
    0 0 0 1 0 1 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 0 1 0 0 0;
    0 0 1 0 0 0 1 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 1 1 1 1 1 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 1 0 1 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 1 0 1 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 1 0 0 0 1 1 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 1 1 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 1 1 0 0 0 1 1 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 1 0 0 0 1 1 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 1 1 0 0 0 1 1 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 0 1 1 1 1 1 0 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]

   push!(boards, board)
   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 1 1 1 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 0 1 1 1 1 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 1 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 1 1 1 1 0 0;
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 1 1 0 1 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 1 0 1 1 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 1 1 1 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 0 1 0 0 0;
    0 0 1 0 0 0 1 0 0;
    0 1 0 0 0 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 1 1 0 0 0;
    0 0 1 0 1 0 1 0 0;
    0 1 0 0 1 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
    0 0 1 0 0 1 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 1 0 0 0 0 0;
    0 0 0 1 0 0 0 0 0;
    0 0 0 1 0 0 0 0 0;
    0 0 0 0 1 1 1 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 1 1 1 0 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 1 1 1 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 1 0 0 0 1 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 0 0 1 0 0 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 0 0 1 0 0 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 1 1 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 1 1 1 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 1 0 0 0 1 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   board = [
    0 0 0 0 0 0 0 0 0;
    0 1 0 0 1 0 0 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 1 0 0 0 1 1 0;
    0 0 0 0 1 0 0 0 0;
    0 0 0 0 1 0 0 0 0;
    0 1 0 0 1 0 0 1 0;
    0 0 0 0 0 0 0 0 0;
   ]
   push!(boards, board)

   save("exp_robots_9_nn_multi.jld", "data", boards)
end

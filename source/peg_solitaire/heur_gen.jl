using JLD, Random
include("PruningSolver.jl")

function generate_heur()
    b = load("boards.jld")["data"]
    boards = b[randperm(length(b))]
    imgs = []
    labels = []
    for i in 1:100
        println(i)
        res, gen = prune_solve(boards[i], 1000)
        for j in CartesianIndices(gen)
            push!(imgs, gen[j])
            push!(labels, getAttackersCount(gen[j]))
        end
    end
    final_imgs = zeros(9,9,3,length(imgs))
    for i in 1:length(imgs)
        m = transfrom_for_network(imgs[i])
        final_imgs[:,:,:,i] = m
    end
    save("peg_solitaire_heur.jld", "data", final_imgs)
    save("peg_solitaire_heur_labels.jld", "data", labels)

    return imgs, labels
end

cells = Dict(0 => [1,0,0], 1 => [0,1,0], 2 => [0,0,1])

function transfrom_for_network(x)
    o = zeros(Float32, size(x)..., 3)
    for ii in CartesianIndices(x)
        o[ii,:,:,:] .= cells[x[ii]]
    end
    return o
end
using JLD, Random
include("PruningSolver.jl")

function exp_standard_heur()
    boards = load("boards_exp.jld")["data"]
    for_constant(boards, 1000, false)
    for_constant(boards, 500, false)
    for_constant(boards, 250, false)
    for_constant(boards, 50, false)
end

function exp_nn_heur()
    boards = load("boards_exp.jld")["data"]
    for_constant(boards, 50, true)
    for_constant(boards, 250, true)
    for_constant(boards, 500, true)
    for_constant(boards, 1000, true)
end

function exp_solv()
    boards = load("solvable.jld")["data"]
    for_constant(boards, 50, true)
    for_constant(boards, 50, false)
end

function for_constant(boards, constant, nn)
    i = 0
    time = 0
    wrong = 0
    right = 0
    solvable = []
    exp = 0
    for board in boards
        solution = @timed prune_solve(board, constant, nn)
        time = time + solution[2]
        if solution[1][1] == false
            wrong = wrong + 1
        else
            exp = exp + solution[1][3]
            right = right + 1
        end
        i = i + 1
        print(string(i, " "))
    end 
    println(string("NN: ", nn, " Constant: ", constant))
    println(time/i)
    println(wrong)
    println(exp/right)
end

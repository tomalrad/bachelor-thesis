using Flux, OhMyREPL, CSV, MLDataUtils, Statistics, LinearAlgebra, BSON, JLD, CUDAnative, CuArrays
using Flux: @epochs, throttle
using BSON: @save, @load


# ARGS: k, conv_size, no_convs, epochs, dev
args = parse.(Int, ARGS)
k = 128 #args[1]
c = 3 #args[2]
no_convs = 3 #args[3]
no_epochs = 50 #args[4]
#dev_no = args[5]
CUDAnative.device!(1)
p=1
if c == 5
    p = 2
elseif c == 7
    p = 3
end

imgs = load("peg_solitaire_step_10.jld")["data"] |> gpu
labels = load("peg_solitaire_step_labels_10.jld")["data"] |> gpu

#shuffled = shuffleobs((imgs,labels))
#trn, tst = splitobs(shuffled, at=1)

train_batches = RandomBatches((imgs,labels), 10000, 100) 

if no_convs == 3
    model = Chain(
        Conv((c,c), 3 => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => 3, identity, pad = (p,p)),
    ) |> gpu
elseif no_convs == 5
    model = Chain(
        Conv((c,c), 3 => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => 2*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 2*k => 3*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 3*k => 3, identity, pad = (p,p)),
    ) |> gpu
elseif no_convs == 7
    model = Chain(
        Conv((c,c), 4 => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), k => 2*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 2*k => 3*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 3*k => 4*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 4*k => 4*k, relu, pad = (p,p)),
        Dropout(0.2),
        Conv((c,c), 4*k => 4, identity, pad = (p,p)),
    ) |> gpu
end

composer = Chain(
    Conv((1,1), 6 => k, swish),
    Conv((1,1), k => 3, identity),
) |> gpu

resnet(model, composer, x) = composer(cat(x, model(x), dims = 3))
tensor2mat(x) = reshape(permutedims(x, (3,1,2,4)), size(x,3), :)
loss(x,y) = Flux.logitcrossentropy(tensor2mat(resnet(model, composer, x)),tensor2mat(y))

opt = ADAM()
ps = Flux.params(model)
push!(ps, Flux.params(composer)...)

tb = getobs(train_batches)
CuArrays.allowscalar(false)
@epochs no_epochs Flux.train!(loss, ps, tb, opt)
Flux.testmode!(model, true)
 
model_name = string("peg_",k,"_",c,"_",p,"_",no_epochs,"_model.bson")
composer_name = string("peg_",k,"_",c,"_",p,"_",no_epochs,"_composer.bson")

# Save model 
m = cpu(model)
c = cpu(composer)

@save model_name m
@save composer_name c
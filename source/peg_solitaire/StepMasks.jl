using LinearAlgebra

function getMasks()
    masks = []
    mask = [
        0 0 2 2
        0 0 2 2
        0 0 2 2
        0 0 2 2
    ]
    pushAll(masks, mask)
    mask = [
        2 0 0 0
        2 0 0 0
        2 0 0 0
        2 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        2 0 0 0
        0 0 0 0
        0 0 0 0
        0 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        2 2 0 0
        2 2 0 0
        0 0 0 0
        0 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        2 2 2 0
        2 2 2 0
        2 2 2 0
        0 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        2 0 0 0
        2 2 2 0
        2 0 0 0
        2 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        2 0 0 0
        2 0 0 0
        2 2 2 0
        2 0 0 0
    ]
    pushAll(masks, mask)
    mask = [
        0 2 0 0
        0 2 0 0
        0 2 0 0
        0 2 0 0
    ]
    pushAll(masks, mask)
    mask = [
        0 0 0 0
        0 0 0 0
        0 2 2 0
        0 2 2 0
    ]
    pushAll(masks, mask)
    mask = [
        0 0 0 0
        0 2 2 0
        0 0 0 0
        0 0 0 0
    ]
    pushAll(masks, mask)
end

function pushAll(masks, mask)
    push!(masks, mask)
    push!(masks, rotate90(mask))
    push!(masks, rotate90(rotate90(mask)))
    push!(masks, rotate90(rotate90(rotate90(mask))))
end

function rotate90(board)
    new_board = deepcopy(board)
    i = size(board)[1]
    for row in eachrow(board)
        new_board[:,i] = row
        i = i-1
    end
    return new_board
end
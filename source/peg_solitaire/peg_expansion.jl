using Flux, OhMyREPL, CSV, MLDataUtils, Statistics, LinearAlgebra, BSON, JLD, CUDAnative
using Flux: @epochs, throttle
using BSON: @save, @load

# BLAS.set_num_threads(4)
#CuArrays.allowscalar(false)
#CUDAnative.device!(3) 

#include("networks/utils_heur.jl")

# imgs, labels = load_prim_8_goal_step(8) |> gpu
imgs = load("peg_solitaire_step.jld")["data"] #|> gpu
labels = load("peg_solitaire_step_labels.jld")["data"] #|> gpu
shuffled = shuffleobs((imgs,labels))
trn, tst = splitobs(shuffled, at=0.9)

train_batches = RandomBatches(trn, 10000, 100)
# test_imgs = Array{Float32}(test_imgs)
# test_labels = Array{Float32}(test_labels)

k = 64
model = Chain(
    Conv((3,3), 3 => k, relu, pad = (1,1)),
    Dropout(0.2),
    Conv((3,3), k => k, relu, pad = (1,1)),
    Dropout(0.2),
    Conv((3,3), k => 3, identity, pad = (1,1)),
) #|> gpu

composer = Chain(
    Conv((1,1), 8 => k, swish),
    Conv((1,1), k => 4, identity),
) #|> gpu

resnet(model, composer, x) = composer(cat(x, model(x), dims = 3))
tensor2mat(x) = reshape(permutedims(x, (3,1,2,4)), size(x,3), :)
loss(x,y) = Flux.logitcrossentropy(tensor2mat(resnet(model, composer, x)),tensor2mat(y))

opt = ADAM()
ps = Flux.params(model)
push!(ps, Flux.params(composer)...)

tb = getobs(train_batches)
@epochs 250 Flux.train!(loss, ps, tb, opt)
Flux.testmode!(model, true)

CuArrays.allowscalar(true)

# Testing the model
o = softmax(resnet(model, composer, cu(test_imgs)), dims=3)
check_walls(o, cu(test_labels))
check_feasible_steps(cu(test_imgs),o,cu(test_labels))

# rand_ixs = rand(1:1:size(imgs,4), 10000)
# rand_imgs = imgs[:,:,:,rand_ixs]
# rand_labels = labels[:,:,:,rand_ixs]
# o = sf(resnet(model, composer, rand_imgs), 3)
# check_walls(o, rand_labels)
# check_feasible_steps(rand_imgs,o,rand_labels)

# Save model 
m = cpu(model)
c = cpu(composer)

@save "resnet_models/maze8_resnet_model_new_data_1.bson" m
@save "resnet_models/maze8_resnet_composer_new_data_1.bson" c
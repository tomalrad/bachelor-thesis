using Flux, OhMyREPL, CSV, MLDataUtils, Statistics, LinearAlgebra, BSON, JLD, CUDAnative, CuArrays
using Flux: @epochs, throttle
using BSON: @save, @load
import Base.minimum
import Base.maximum

#include("utils.jl")

# ARGS = pad with walls [0,1], no_atts in block[10,?], att_blocks[1,5], epochs[?], gpu_no[0,1,2,3]
#args = parse.(Int, ARGS)
args = [0 5 1 50 2]
dev_no = args[5]
CUDAnative.device!(dev_no) 

wall_pad = args[1]
imgs = load("peg_solitaire_heur_1.jld")["data"] |> gpu
labels = load("peg_solitaire_heur_labels_1.jld")["data"] |> gpu

println(size(imgs))


no_batches = 10000  
batch_size = 60
no_mzs_in_block = 60
batches = RandomBatches((imgs,labels), 10000, 50)  #make_batches(imgs, labels, no_batches, batch_size)##
batch_size_identity = zeros(no_mzs_in_block, no_mzs_in_block) + I |> gpu 

CuArrays.allowscalar(false) 

"""function get_attentions(no_atts, no_att_blocks)
    attention_blocks = []
    ps = []
    for i in 1:no_att_blocks
        att = []
        for j in 1:no_atts
            if i == 1 && wall_pad == 1
                l = Chain(Conv((3,3), 3 => 1, swish), x -> softmax(x,dims=[1,2])) |> gpu
            else 
                l = Chain(Conv((3,3), 3 => 1, pad=(1,1), swish), x -> softmax(x,dims=[1,2])) |> gpu
            end
            push!(att, l)
            push!(ps, params(l)...)
        end
        push!(attention_blocks, att)
    end
    return attention_blocks, ps
end

apply_atts(x, atts) = cat(cat([x .* a(x) for a in atts]..., dims=3), add_coord_filters(x), dims=3) # vraci no_atts * x_filters + x_filters + 2 filtru -> proste to co chceme 
apply_atts_wrap(x, atts) = cat(cat([x[2:8,2:8,:,:] .* a(x) for a in atts]..., dims=3), add_coord_filters(x[2:8,2:8,:,:]), dims=3)

no_atts = args[2]
att_blocks = args[3]
atts, ps = get_attentions(no_atts, att_blocks)
no_filters = no_atts * 3 + 5 """

"""if att_blocks == 1 && wall_pad == 1
    model = Chain(
        x -> apply_atts_wrap(x, atts[1]),
        Conv((1,1), no_filters => 24, swish),
        Conv((3,3), 24 => 24, pad=(1,1), swish),
        Conv((3,3), 24 => 48, pad=(1,1), swish),
        Conv((3,3), 48 => 96, pad=(1,1), swish),
        x -> sum(x, dims=[1,2]),    
        x -> reshape(x, :, size(x)[4]),
        Dense(96, 1),
        # x -> softplus.(x)
    ) |> gpu
elseif att_blocks == 1 && wall_pad == 0
    model = Chain(
        x -> apply_atts(x, atts[1]),
        Conv((1,1), no_filters => 24, swish),
        Conv((3,3), 24 => 24, pad=(1,1), swish),
        Conv((3,3), 24 => 48, pad=(1,1), swish),
        Conv((3,3), 48 => 96, pad=(1,1), swish),
        x -> sum(x, dims=[1,2]),    
        x -> reshape(x, :, size(x)[4]),
        Dense(96, 1),
        # x -> psycho_sigmoid.(x)
        # Dense(1, 1)        
        # x -> sigmoid.(x)
        # x -> softplus.(x)
    ) |> gpu
end"""
h = 32
model = Chain(
    Conv((1,1), 3=>32, swish),
    Conv((3,3), 32=>32, pad=(1,1), swish),
    Conv((3,3), 32=>48, pad=(1,1), swish),
    Conv((3,3), 48 => 96, pad=(1,1), swish),
    x -> sum(x, dims=[1,2]),    
    x -> reshape(x, :, size(x)[4]),
    Dense(96, h),
    Dense(h,h,relu),
    Dense(h,h,relu),
    Dense(h,1)
) |> gpu

ps = Flux.params(model)
 
function loss(x,y)
    mx = model(x) 
    data_diffs = -1 .* (mx .- transpose(mx)) |> gpu
    label_diffs = sign.(y .- transpose(y)) |> gpu
    mlt = (data_diffs .* label_diffs) .+ 1 |> gpu
    rl = relu.(mlt) |> gpu
    tmp = rl #.- batch_size_identity
    return sum(tmp)
end


opt = ADAM()
no_epochs = args[4]
tb = getobs(batches)
@epochs no_epochs Flux.train!(loss, ps, tb, opt)


CuArrays.allowscalar(true)
model_name = string("heur_conv",no_epochs,"_epochs")
#mkdir(string("experiments/",model_name))
# Save plots 
"""for i in 1:10
    r = rand(1:size(imgs)[4])
    maze = Float32.(onehot2img(cpu(imgs[:,:,1:4,r:r])))
    save_monotonic_att(model, maze, string("experiments/",model_name,"/plots/tr",i,".png"))
end
println("Saved train plots")
for i in 1:10
    r = rand(1:size(test_imgs)[4])
    maze = Float32.(onehot2img(cpu(test_imgs[:,:,1:4,r:r])))
    save_monotonic_att(model, maze, string("experiments/",model_name,"/plots/te",i,".png"))
end
println("Saved test plots")
uglies = get_ugly_mazes()
for i in 1:length(uglies)
    if wall_pad == 1
        maze = onehot2img(wrap_with_walls(uglies[i]))
    else 
        maze = uglies[i]
    end
    save_monotonic_att(model, maze, string("experiments/",model_name,"/plots/u",i,".png"))
end
println("Saved ugly plots")

# Save stats into txt 
open(string("experiments/",model_name,"/stats.txt"), "w") do f 
    write(f, string("Train loss = ", train_loss, "\n"))
    write(f, string("Test loss = ", test_loss, "\n"))
end 
println("Saved stats")"""

# Save model and attentions 
m = cpu(model)
@save string(model_name,".bson") m

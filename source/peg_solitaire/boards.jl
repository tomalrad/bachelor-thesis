using JLD, CUDAnative, Random
include("solver.jl")
function getBoards()
    boards = []
    board = [
        2 2 2 2 2 2 2 2 2;
        2 2 2 2 2 2 2 2 2;
        2 2 2 2 0 2 2 2 2;
        2 2 2 2 0 2 2 2 2;
        2 2 2 2 1 2 2 2 2;
        2 2 2 2 1 2 2 2 2;
        2 2 2 2 2 2 2 2 2;
        2 2 2 2 2 2 2 2 2;
        2 2 2 2 2 2 2 2 2;
    ]
    push!(boards, board)
    board = [
        2 2 2 2 2 2 2 2 2;
        2 2 2 1 1 1 2 2 2;
        2 2 2 1 1 1 2 2 2;
        2 1 1 1 1 1 1 1 2;
        2 1 1 1 0 1 1 1 2;
        2 1 1 1 1 1 1 1 2;
        2 2 2 1 1 1 2 2 2;
        2 2 2 1 1 1 2 2 2;
        2 2 2 2 2 2 2 2 2;
    ]
    push!(boards, board)
    board = [
        2 2 2 2 2 2 2 2 2;
        2 2 2 1 1 1 2 2 2;
        2 2 1 1 1 1 1 2 2;
        2 1 1 1 0 1 1 1 2;
        2 1 1 1 1 1 1 1 2;
        2 1 1 1 1 1 1 1 2;
        2 2 1 1 1 1 1 2 2;
        2 2 2 1 1 1 2 2 2;
        2 2 2 2 2 2 2 2 2;
    ]
    push!(boards, board)
    board = [
        2 2 2 2 2 2 2 2 2 2 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 1 1 1 1 1 1 1 1 1 2;
        2 1 1 1 1 0 1 1 1 1 2;
        2 1 1 1 1 1 1 1 1 1 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 2 2 2 1 1 1 2 2 2 2;
        2 2 2 2 2 2 2 2 2 2 2;
    ]
    push!(boards, board)
    return boards
end

function generateSquare(width)
    arr = ones(Int8, width, width)
    for i in CartesianIndices(arr)
        if(i[1] == 1 || i[2] == 1 || i[1] == width || i[2] == width)
            arr[i] = 2
        end
    end
    arr[width÷2 + 1, width÷2 + 1] = 0
    return arr
end

function generateCross(width)
    arr = generateSquare(width)
    offset = width÷2 - 1
    for i in CartesianIndices(arr)
        if i[1] <= offset && i[2] <= offset
           arr[i] = 2
        end
        if i[1] <= offset && i[2] > width-offset
            arr[i] = 2
        end
        if i[1] > width-offset && i[2] <= offset
            arr[i] = 2
        end
        if i[1] > width-offset && i[2] > width-offset
            arr[i] = 2
        end
    end
    return arr 
end

function generate_random(size)
     frame = fill(2, (size,size))
     board = rand([1,2], size-2, size-2)
     start1 = rand(1:size-2)
     start2 = rand(1:size-2)
     board[start1, start2] = 0
     for i in CartesianIndices(board)
        frame[CartesianIndex(i[1] + 1, i[2] + 1)] = board[i]
     end
     return frame
end

function generateTestBoards()
    """res = []
    while size(res)[1] < 5
        board = generate_random(9)
        solution = prune_solve(board, 1000)
        if solution != false
            push!(res, board)
            println(size(res)[1])
        end
    end   
    #save("cdata10x10.jld", "data", res)
    return res"""
    res = []
    visited = Set()
    before = load("data9x9_53.jld")["data"]   
    for board in before
        push!(visited, board)
    end
    before3 = load("data9x9_6.jld")["data"]   
    for board in before3
        push!(visited, board)
    end
    before2 = load("data9x9_1.jld")["data"]
    for board in before2
        push!(visited, board)
    end
    before4 = load("data9x9_41.jld")["data"]
    for board in before4
        push!(visited, board)
    end
    before5 = load("data9x9_98.jld")["data"]
    for board in before5
        push!(visited, board)
    end
    before6 = load("data9x9_135.jld")["data"]
    for board in before6
        push!(visited, board)
    end
    println(length(visited))
    i = 136
    while length(visited) < 500000
        board = getRandomBoard()
        solution = prune_solve(board, 1000)
        if solution != false && !(solution in visited)
            push!(res, board)
            push!(visited, board)
            println(length(visited))
            if mod(size(res)[1], 100) == 0
                save("data9x9_" * string(i) * ".jld", "data", res)
                i = i + 1
            end
        end
    end
    save("data9x9.jld", "data", res)
    return res
end

function generateData()
    res = []
    before = load("data9x9_53.jld")["data"]   
    for board in before
        push!(res, board)
    end
    before3 = load("data9x9_6.jld")["data"]   
    for board in before3
        push!(res, board)
    end
    before2 = load("data9x9_1.jld")["data"]
    for board in before2
        push!(res, board)
    end
    before4 = load("data9x9_41.jld")["data"]
    for board in before4
        push!(res, board)
    end
    before5 = load("data9x9_98.jld")["data"]
    for board in before5
        push!(res, board)
    end
    before6 = load("data9x9_135.jld")["data"]
    for board in before6
        push!(res, board)
    end
    before7 = load("data9x9_150.jld")["data"]
    for board in before7
        push!(res, board)
    end
    save("boards.jld", "data", res)
end

function getRandomBoard()
    board = generateSquare(9)
    board[1:3, 1:3] = upLeftcornerSegments()[rand(1:end)]
    board[1:3, 4:6] = upMiddleSegments()[rand(1:end)]
    board[1:3, 7:9] = upRightcornerSegments()[rand(1:end)]
    board[4:6, 1:3] = leftMiddleSegments()[rand(1:end)]
    board[4:6, 4:6] = getCenters()[rand(1:end)]
    board[4:6, 7:9] = rightMiddleSegments()[rand(1:end)]
    board[7:9, 1:3] = downRightcornerSegments()[rand(1:end)]
    board[7:9, 4:6] = downMiddleSegments()[rand(1:end)]
    board[7:9, 7:9] = downLeftcornerSegments()[rand(1:end)]
    return board
end

function rotateArray90(array)
    res = []
    for i in CartesianIndices(array)
        rotated = rotate90(array[i])
        push!(res, rotated)
    end
    return res
end

function rotateArray180(array)
    res = []
    for i in CartesianIndices(array)
        rotated = rotate90(rotate90(array[i]))
        push!(res, rotated)
    end
    return res
end

function rotateArray270(array)
    res = []
    for i in CartesianIndices(array)
        rotated = rotate90(rotate90(rotate90(array[i])))
        push!(res, rotated)
    end
    return res
end


function upLeftcornerSegments()
    res = []
    seg = [
        2 2 2;
        2 1 1;
        2 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 1;
        2 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 1;
        2 2 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 2 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 2 2;
    ]
    push!(res, seg)
end

function upRightcornerSegments()
    return rotateArray90(upLeftcornerSegments())
end

function downLeftcornerSegments()
    return rotateArray180(upLeftcornerSegments())
end

function downRightcornerSegments()
    return rotateArray270(upLeftcornerSegments())
end

function upMiddleSegments()
    res = []
    seg = [
        2 2 2;
        1 1 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 1 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 2 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 2 2;
        2 2 2;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 2 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 2 1;
        1 2 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 1 2;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        2 1 2;
        2 1 2;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 1 1;
        1 2 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 1 1;
        2 1 1;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 1 1;
        1 1 2;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 1 1;
        2 1 2;
    ]
    push!(res, seg)
    seg = [
        2 2 2;
        1 1 1;
        2 2 2;
    ]
    push!(res, seg)

end

function getCenters()
    res = []
    seg = [
        1 0 1;
        1 1 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        1 2 1;
        1 1 1;
        1 0 1;
    ]
    push!(res, seg)
    seg = [
        1 2 1;
        1 1 0;
        1 2 1;
    ]
    push!(res, seg)
    seg = [
        2 0 2;
        1 1 1;
        2 1 2;
    ]
    push!(res, seg)
    seg = [
        1 1 1;
        1 1 0;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        1 2 1;
        0 1 1;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        1 2 1;
        0 1 1;
        1 2 1;
    ]
    push!(res, seg)
    seg = [
        2 1 2;
        1 1 0;
        2 1 2;
    ]
    push!(res, seg)
    seg = [
        1 1 1;
        1 1 1;
        1 0 1;
    ]
    push!(res, seg)
    seg = [
        1 2 1;
        1 1 0;
        1 1 1;
    ]
    push!(res, seg)
    seg = [
        2 1 2;
        1 1 1;
        2 0 2;
    ]
    push!(res, seg)
    return res
end

function rightMiddleSegments()
    return rotateArray90(upMiddleSegments())
end

function downMiddleSegments()
    return rotateArray180(upMiddleSegments())
end

function leftMiddleSegments()
    return rotateArray270(upMiddleSegments())
end

function generate3x3()
    boards = []
    for i in 0:512
       board = generateArray(i)[8:16]
       res = []
       for i in CartesianIndices(board)
          push!(res, board[i]+1)
       end
       res = reshape(res, (3, 3))
       res[2, 2] = 0
       push!(boards, res)
    end
    return boards
end

function generateArray(i)
    return parse.(Int64 ,split(bitstring(UInt16(i)),""))
end

function rotate90(board)
    new_board = deepcopy(board)
    i = size(board)[1]
    for row in eachrow(board)
        new_board[:,i] = row
        i = i-1
    end
    return new_board
end
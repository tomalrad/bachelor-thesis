include("solver.jl")
heur_nn, model, composer = load_nn()
apply_atts = (x, atts) -> cat([x .* a(x) for a in atts]...,x,dims=3)

function prune_solve(board, prun_constant, nn)
    expanded = 0
    generation = [board]
    generations = []
    gen_num = 0
    if nn
        next_func = compute_next_gen_nn
    else
        next_func = compute_next_gen
    end
    while true
        #println(gen_num)
        #println(length(generation))
        gen_num = gen_num + 1
        if length(generation) == 0
            return false, nothing, expanded
        end
        generation, expanded = next_func(generation, prun_constant, expanded)
        for i in CartesianIndices(generation)
            push!(generations, generation[i])
        end
        for i in CartesianIndices(generation)
            if checkEnd(generation[i])
                return generation[i], generations, expanded
            end
        end
    end
end

function compute_next_gen_nn(prev_gen, prun_constant, expanded)
    visited = Set()
    q = PriorityQueue{Array{Int64,2}, Float32}()
    for i in CartesianIndices(prev_gen)
        current = prev_gen[i]
        expanded = expanded + 1
        available = getAvailableIndices(current)
        for i in CartesianIndices(available)
            next = getNext(current, available[i])
            for j in CartesianIndices(next)
                if !(next[j] in visited)
                    enqueue!(q, next[j], -1 * get_heur_nn(next[j], heur_nn, apply_atts))
                    push!(visited, next[j])
                    push!(visited, rotate90(next[j]))
                    push!(visited, rotate90(rotate90(next[j])))
                    push!(visited, rotate90(rotate90(rotate90(next[j]))))
                end
            end
        end
    end
    i = 0
    result = []
    while i < prun_constant && length(q) > 0
        i =  i + 1
        cur = dequeue!(q)
        push!(result, cur)
    end
    return result, expanded
end
function compute_next_gen(prev_gen, prun_constant, expanded)
    visited = Set()
    q = PriorityQueue{Array{Int64,2}, Int64}()
    for i in CartesianIndices(prev_gen)
        current = prev_gen[i]
        expanded = expanded + 1
        available = getAvailableIndices(current)
        for i in CartesianIndices(available)
            next = getNext(current, available[i])
            for j in CartesianIndices(next)
                if !(next[j] in visited)
                    enqueue!(q, next[j], 0)
                    push!(visited, next[j])
                    push!(visited, rotate90(next[j]))
                    push!(visited, rotate90(rotate90(next[j])))
                    push!(visited, rotate90(rotate90(rotate90(next[j]))))
                end
            end
        end
    end
    i = 0
    result = []
    while i < prun_constant && length(q) > 0
        i =  i + 1
        cur = dequeue!(q)
        push!(result, cur)
    end
    return result, expanded
end

function getAttackersCount(board)
    count = 0
    for i in CartesianIndices(board)
        if (board[i] == 0)
            attackers = getAttackers(i, board)
            neighbors = getNeighbors(i, board)
            for j in CartesianIndices(attackers)
                if board[attackers[j]] == 1 && board[neighbors[j]] == 1
                    count = count + 1
                end
            end
        end
    end
    return count
end

function rotate90(board)
    new_board = deepcopy(board)
    i = size(board)[1]
    for row in eachrow(board)
        new_board[:,i] = row
        i = i-1
    end
    return new_board
end
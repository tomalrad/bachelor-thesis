using DataStructures, BSON, Flux, CUDAnative, CuArrays
using DataStructures: Queue, PriorityQueue, Stack
using BSON: @load

CUDAnative.device!(1) 

struct State
    board
end

function getAvailableIndices(board)
    return CartesianIndices(board)[board .== 0]
end

function getPlayableIndices(board)
    res = []
    available = getAvailableIndices(board);
    for i in CartesianIndices(available)
        neighbors = getNeighbors(available[i], board)
        attackers = getAttackers(available[i], board)
        #print("Attackers ", size(attackers), "neighbors ", size(neighbors))
        #print("\n")
        if attackers != []
            for j in CartesianIndices(attackers)
                if (checkbounds(Bool, board, attackers[j])) && 
                    (board[neighbors[j]] == 1) && (board[attackers[j]] == 1)
                    push!(res, available[i])
                    break
                end
            end
        end
    end
    return res
end

function getNeighbors(index, board)
    result = []
        if index[1] + 1 < size(board)[1] && index[1] + 2 < size(board)[1]
            if(checkbounds(Bool, board, CartesianIndex(index[1] + 1, index[2])))
                push!(result, CartesianIndex(index[1] + 1, index[2]))
            end
        end
        if index[1] - 1 > 0 && index[1] - 2 > 0
            if (checkbounds(Bool, board, CartesianIndex(index[1] - 1, index[2])))
                push!(result,CartesianIndex(index[1] - 1, index[2]))
            end
        end
        if index[2] + 1 < size(board)[2] && index[2] + 2 < size(board)[2]
            if (checkbounds(Bool, board, CartesianIndex(index[1] , index[2] + 1)))
                push!(result, CartesianIndex(index[1], index[2] + 1))
            end
        end
        if index[2] - 1 > 0 && index[2] - 2 > 0
            if (checkbounds(Bool, board, CartesianIndex(index[1], index[2]-1)))
                push!(result, CartesianIndex(index[1], index[2] - 1))
            end
        end
    return result;
end

function getAttackers(index, board)
    result = []
        if index[1] + 2 < size(board)[1]
            if (checkbounds(Bool, board, CartesianIndex(index[1] + 2, index[2])))
                push!(result, CartesianIndex(index[1] + 2, index[2]))
            end
        end
        if index[1] - 2 > 0
            if (checkbounds(Bool, board, CartesianIndex(index[1] - 2, index[2])))
                push!(result,CartesianIndex(index[1] - 2, index[2]))
            end
        end
        if index[2] + 2 < size(board)[2]
            if (checkbounds(Bool, board, CartesianIndex(index[1], index[2] + 2)))
                push!(result, CartesianIndex(index[1], index[2] + 2))
            end
        end
        if index[2] - 2 > 0
            if (checkbounds(Bool, board, CartesianIndex(index[1], index[2] - 2)))
                push!(result, CartesianIndex(index[1], index[2] - 2))
            end
        end
    return result;
end

function getNext(board, index)
    result = []
    #println(index)
    attackers = getAttackers(index, board)
    neighbors = getNeighbors(index, board)
    #println(attackers)
    for i in CartesianIndices(attackers)
        if(board[attackers[i]] == 1 && board[neighbors[i]] == 1)
            newBoard = deepcopy(board)
            #println(attacker)
            newBoard[attackers[i]] = 0
            newBoard[neighbors[i]] = 0 
            newBoard[index] = 1
            #printBoard(newBoard)
            push!(result, newBoard)
        end
    end
    #for j in CartesianIndices(result)
     #   printBoard(result[j])
      #  println("================================================================")
    #end
    return result
end

function load_nn()
    #@load "peg_128_3_1_50_model.bson" m
    #@load "peg_128_3_1_50_composer.bson" c
    @load "heur_conv50_epochs.bson" m

    #model = m #|> gpu
    #composer = c #|> gpu
    heur_nn = m |> gpu
    return heur_nn, nothing, nothing
end

function load_heur_nn()
    @load "peg_heur_100_onehot_model.bson" m

    atts = []

    @load "att1_100_onehot.bson" a1
    @load "att2_100_onehot.bson" a2
    @load "att3_100_onehot.bson" a3
    @load "att4_100_onehot.bson" a4
    @load "att5_100_onehot.bson" a5

    att_block = []
    push!(att_block, a1 |> gpu)
    push!(att_block, a2 |> gpu)
    push!(att_block, a3 |> gpu)
    push!(att_block, a4 |> gpu)
    push!(att_block, a5 |> gpu)
    push!(atts, att_block)

    model = m |> gpu   
   
    return model, atts
end

function get_next_nn(board, model, composer)
    data = reshape(transfrom_for_network(board), size(board,1), size(board,2), 3, 1)
    r = softmax(resnet(model, composer, data), dims=3)
    #boards = from_network_rep(r)
    #printBoard(boards[:, :, 1])
    return r
end

function get_heur_nn(board, heur, apply_atts)
    data = reshape(transfrom_for_network(board), size(board,1), size(board,2), 3, 1) |> gpu
    CuArrays.allowscalar(false)
    val = heur(data)
    CuArrays.allowscalar(true)
    return cpu(val)[1]
end

function from_network_rep(network_rep)
    boards = ones(Int8, size(network_rep,1),size(network_rep,2),size(network_rep,4))
    for i in 1:size(network_rep, 4)
        for j in CartesianIndices(network_rep[:,:,1,i])
            if round(network_rep[j, 1, i]) == 1 
                boards[j, i] = 0
            elseif round(network_rep[j, 3, i]) == 1
                boards[j, i] = 2
            end
        end
    end
    return boards
end

function getNextTest(board)
    model, composer = load_nn()
    next = get_next_nn(board, model, composer)
    boards = generate_steps_from_nn(board, next)
    for i in CartesianIndices(boards)
        printBoard(boards[i])
    end
    return boards
end

function generate_steps_from_nn(board, nn_input)
    res = []
    #println("here")
    #println(nn_input)
     available = getAvailableIndices(board)
     #println(available)
     for i in CartesianIndices(available)
        println("One prob: ", nn_input[available[i][1], available[i][2], 2, 1])
        println("Zero prob ", nn_input[available[i][1], available[i][2], 1, 1])
        #println(nn_input[available[i][1], available[i][2], 2, 1])
        if nn_input[available[i][1], available[i][2], 2, 1] > 0.5 || nn_input[available[i][1], available[i][2], 1, 1] < 0.1
            println("One prob: ", nn_input[available[i][1], available[i][2], 2, 1])
            println("Zero prob ", nn_input[available[i][1], available[i][2], 1, 1])
            attackers = getAttackers(available[i], board)
            neighbors = getNeighbors(available[i], board) 
            for j in CartesianIndices(attackers)
                """nn_input[attackers[j][1], attackers[j][2], 1, 1] > 0.9 &&"""
                if board[neighbors[j]] == 1 && board[attackers[j]] == 1
                    newBoard = deepcopy(board)
                    newBoard[attackers[j]] = 0
                    newBoard[neighbors[j]] = 0 
                    newBoard[available[i]] = 1
                    push!(res, newBoard)
                end
            end
        end
     end
     return res
end

cells = Dict(0 => [1,0,0], 1 => [0,1,0], 2 => [0,0,1])

function transfrom_for_network(x)
    o = zeros(Float32, size(x)..., 3)
    for ii in CartesianIndices(x)
        o[ii,:,:,:] .= cells[x[ii]]
    end
    return o
end

function resnet(model,composer,x)

    composer(cat(x, model(x), dims = 3))

end

function checkEnd(board)
    ret = false
    first = true
    for i in CartesianIndices(board)
        if (board[i] == 1) && first
            ret = true
            first = false
        elseif board[i] == 1
            ret = false
        end
    end
    return ret
end

function checkSum(board)
    ret = 0;
    for i in CartesianIndices(board)
        if board[i] == 1
            ret = ret + 1
        end
    end
    return ret
end

function getAttackersCount(board)
    count = 0
    for i in CartesianIndices(board)
        if (board[i] == 0)
            attackers = getAttackers(i, board)
            neighbors = getNeighbors(i, board)
            for j in CartesianIndices(attackers)
                if board[attackers[j]] == 1 && board[neighbors[j]] == 1
                    count = count + 1
                end
            end
        end
    end
    return count
end
function solve(board)
    q = PriorityQueue{Array{Int64,2}, Int64}()
    
    visited = Set()
    #printBoard(board)
    enqueue!(q,board, checkSum(board))
    #push!(visited, board)
    iter = 0
    while length(q) > 0
        iter = iter + 1
        current = dequeue!(q)
        #println(checkSum(current))
        if checkEnd(current)
            println(iter)
            return current
        end
        #if iter > size(board)[1]*size(board)[1]*1000
            #return false
        #end
        available = getAvailableIndices(current)
        for i in CartesianIndices(available)
            next = getNext(current, available[i])
            for j in CartesianIndices(next)
                if !(next[j] in visited)
                    enqueue!(q, next[j], -1 * getAttackersCount(next[j]))
                    push!(visited, next[j])
                    reversed = next[j][end:-1:1,end:-1:1]
                    push!(visited, reversed)
                    transpn = transpose(next[j])
                    transpr = transpose(reversed)
                    push!(visited, transpn)
                    push!(visited, transpr)
                    #printBoard(next[j])
                end
            end
        end
    end
    return false
end

function solveStack(board)
    q = Stack{Array{Int64,2}}()
    
    visited = Set()
    #printBoard(board)
    push!(q,board)
    #push!(visited, board)
    iter = 0
    while length(q) > 0
        iter = iter + 1
        current = pop!(q)
        #println(checkSum(current))
        if checkEnd(current)
            println(iter)
            return current
        end
        #if iter > size(board)[1]*size(board)[1]*1000
            #return false
        #end
        available = getAvailableIndices(current)
        for i in CartesianIndices(available)
            next = getNext(current, available[i])
            for j in CartesianIndices(next)
                if !(next[j] in visited)
                    push!(q, next[j])
                    push!(visited, next[j])
                    reversed = next[j][end:-1:1,end:-1:1]
                    push!(visited, reversed)
                    transpn = transpose(next[j])
                    transpr = transpose(reversed)
                    push!(visited, transpn)
                    push!(visited, transpr)
                    #printBoard(next[j])
                end
            end
        end
    end
    return false
end



function getDetached(board)
    detached = 0
    for i in CartesianIndices(board)
        if board[i] == 1
            neighbors = getNeighbors(i, board)
            vals = []
            for j in neighbors
                push!(vals, board[j])
            end
            if !(1 in vals)
                detached = detached + 1
            end
        end
    end
    return detached
end


    
function printBoard(board)
    for row in eachrow(board)
        println(row)
    end
    println("")
end
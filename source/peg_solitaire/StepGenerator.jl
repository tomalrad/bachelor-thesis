using JLD

include("solver.jl")
include("StepMasks.jl")

function generate4x4()
    boards = []
    for i in 0:65535
       board = reshape(generateArray(i), (4 , 4))
       masks = getMasks()
       for j in CartesianIndices(masks)
          push!(boards, applyMask(masks[j], board))
       end
       push!(boards, board)
    end
    return boards
end

function generate5x5()
    boards = []
    for i in 0:33554432
       board = reshape(generateArray(i), (5 , 5))
       masks = getMasks()
       for j in CartesianIndices(masks)
          push!(boards, applyMask(masks[j], board))
       end
       push!(boards, board)
    end
    return boards
end

function generateArray(i)
    return parse.(Int64 ,split(bitstring(UInt16(i)),""))
end


function getTrainingData()
    data = Vector{Vector{Array{Int64,2}}}([])
    boards = generate4x4();
    for i in CartesianIndices(boards)
        available = getPlayableIndices(boards[i])
        for j in CartesianIndices(available)
            next = getNext(boards[i], available[j])
            for k in CartesianIndices(next)
                row = []
                push!(row, boards[i])
                push!(row, next[k])
                push!(data, row)
            end
        end
    end
    return data
end

function saveTrainingData()
    data = getTrainingData()
    imgs = []
    labels = []
    for sample in data 
        push!(imgs, sample[1])
        push!(labels, sample[2])
    end
    final_imgs = zeros(4,4,3,length(imgs))
    final_labels = zeros(4,4,3,length(labels))
    for i in 1:length(imgs)
        m = transfrom_for_network(imgs[i])
        l = transfrom_for_network(labels[i])
        final_imgs[:,:,:,i] = m
        final_labels[:,:,:,i] = l
    end
    save("peg_solitaire_step_10.jld", "data", final_imgs)
    save("peg_solitaire_step_labels_10.jld", "data", final_labels)
    return labels
end

function applyMask(mask, board)
    new_board = [
        2 2 2 2
        2 2 2 2
        2 2 2 2
        2 2 2 2
    ]
    for i in CartesianIndices(mask)
        if mask[i] != 2
            new_board[i] = board[i]
        end
    end
    return new_board
end

cells = Dict(0 => [1,0,0], 1 => [0,1,0], 2 => [0,0,1])

function transfrom_for_network(x)
    o = zeros(Float32, size(x)..., 3)
    for ii in CartesianIndices(x)
        o[ii,:,:,:] .= cells[x[ii]]
    end
    return o
end
